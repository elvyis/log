# 简单易用的跨平台C/C++日志库，支持linux、windows、嵌入式rtos、以及嵌入式裸机

#### 介绍

- 跨平台C/C++日志库，支持linux、windows、嵌入式rtos、以及嵌入式裸机；
- 支持十六进制打印；
- 支持终端颜色显示；
- 自定义注册日志输出，由此可将日志信息输出到控制台、文件、以及其他设备，如在单片机中保存至FLASH。

#### 移植方法

- `log_timestamp`函数用来给日志提供时间显示，如果在单片机中不支持时间，那么该函数可直接返回空串""。

- `LOG_ENABLED`宏用于使能全局日志的使能

- `LOG_HEXDUMP_LINE_ALIGN`宏用于控制HEX打印一行的数据量

- 以下宏定义用于适配互斥访问：
	```c
	#define hmutex_t                  void*
	#define hmutex_init(mutex)        ((void)mutex)
	#define hmutex_destroy(mutex)     ((void)mutex)
	#define hmutex_lock(mutex)        ((void)mutex)
	#define hmutex_unlock(mutex)      ((void)mutex)
	```

#### 使用方法

```c
#include <stdio.h>
#include <stdlib.h>
#include "log.h"

int main()
{
	int i;
	int arg = 1209;
	uint8_t buf[128];

	memset(buf, 0XAA, sizeof(buf));

	LOG_INIT();

	// 注册日志输出到控制台
	LOG_SUBSCRIBE(log_console_output, LOG_TRACE_LEVEL);

	// 注册日志输出到文件
	LOG_SUBSCRIBE(log_file_output, LOG_TRACE_LEVEL);

//	LOG_SUBSCRIBE(log_console_output, LOG_DEBUG_LEVEL);
//	LOG_SUBSCRIBE(log_console_output, LOG_INFO_LEVEL);
//	LOG_SUBSCRIBE(log_console_output, LOG_WARNING_LEVEL);
//	LOG_SUBSCRIBE(log_console_output, LOG_ERROR_LEVEL);
//	LOG_SUBSCRIBE(log_console_output, LOG_CRITICAL_LEVEL);

	// 原始输出
	LOG(LOG_INFO_LEVEL, "this is message ! \n");

	// 带TAG的输出
	LOGF("this is message, arg:%d !", arg);
	LOGE("this is message, arg:%d !", arg);
	LOGW("this is message, arg:%d !", arg);
	LOGI("this is message, arg:%d !", arg);
	LOGD("this is message, arg:%d !", arg);
	LOGT("this is message, arg:%d !", arg);

	for(i = 0; i < sizeof(buf); i++)
		buf[i] = rand() % 0XFF;

	// 显示内存地址并4字节对齐的打印
	LOG_HEXDUMP(LOG_INFO_LEVEL, buf, sizeof(buf), 1);
	LOG(LOG_INFO_LEVEL, "\n");

	// 显示内存地址并4字节对齐的打印（测试打印非4字节对齐的buf）
	LOG_HEXDUMP(LOG_INFO_LEVEL, buf + 1, sizeof(buf) - 1, 1);
	LOG(LOG_INFO_LEVEL, "\n");

	// 原始打印
	LOG_HEXDUMP(LOG_INFO_LEVEL, buf, sizeof(buf), 0);
	LOG(LOG_INFO_LEVEL, "\n");

    return 0;
}
```

#### 运行效果

输出到文件和控制台显示效果：

![](result.png)


