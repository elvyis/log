#include <stdio.h>
#include <stdlib.h>
#include "log.h"

int main()
{
	int i;
	int arg = 1209;
	uint8_t buf[128];

	memset(buf, 0XAA, sizeof(buf));

	LOG_INIT();

	// 注册日志输出到控制台
	LOG_SUBSCRIBE(log_console_logger, LOG_TRACE_LEVEL);

	// 注册日志输出到文件
	LOG_SUBSCRIBE(log_file_logger, LOG_TRACE_LEVEL);

//	LOG_SUBSCRIBE(log_console_logger, LOG_DEBUG_LEVEL);
//	LOG_SUBSCRIBE(log_console_logger, LOG_INFO_LEVEL);
//	LOG_SUBSCRIBE(log_console_logger, LOG_WARNING_LEVEL);
//	LOG_SUBSCRIBE(log_console_logger, LOG_ERROR_LEVEL);
//	LOG_SUBSCRIBE(log_console_logger, LOG_CRITICAL_LEVEL);

	// 原始输出
	LOG(LOG_INFO_LEVEL, "this is message ! \n");

	// 带TAG的输出
	LOGF("this is message, arg:%d !", arg);
	LOGE("this is message, arg:%d !", arg);
	LOGW("this is message, arg:%d !", arg);
	LOGI("this is message, arg:%d !", arg);
	LOGD("this is message, arg:%d !", arg);
	LOGT("this is message, arg:%d !", arg);

	for(i = 0; i < sizeof(buf); i++)
		buf[i] = rand() % 0XFF;

	// 显示内存地址并4字节对齐的打印
	LOG_HEXDUMP(LOG_INFO_LEVEL, buf, sizeof(buf), 1);
	LOG(LOG_INFO_LEVEL, "\n");

	// 显示内存地址并4字节对齐的打印（测试打印非4字节对齐的buf）
	LOG_HEXDUMP(LOG_INFO_LEVEL, buf + 1, sizeof(buf) - 1, 1);
	LOG(LOG_INFO_LEVEL, "\n");

	// 原始打印
	LOG_HEXDUMP(LOG_INFO_LEVEL, buf, sizeof(buf), 0);
	LOG(LOG_INFO_LEVEL, "\n");

    return 0;
}
